;; add to path
(add-to-list 'load-path "~/.emacs.d/spacemacs-theme")
(add-to-list 'load-path "~/.emacs.d/dash")
(add-to-list 'load-path "~/.emacs.d/solarized-theme")
(add-to-list 'load-path "~/.emacs.d/doom-theme")
(add-to-list 'load-path "~/.emacs.d/powerline")
(add-to-list 'load-path "~/.emacs.d/smooth-scrolling")
(add-to-list 'load-path "~/.emacs.d/linum-relative")
(add-to-list 'load-path "~/.emacs.d/ivy")

;; requires
(require 'rx)
(require 'package)

;; melpa packages
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

;; more requires
(require 'doom-themes)
(require 'spacemacs-common)
(require 'dash)
(require 'solarized-theme)
(require 'powerline)
(require 'multiple-cursors)
(require 'smartparens-config)
(require 'smooth-scrolling)
(require 'linum-relative)
(require 'ivy)
(require 'swiper)
(require 'counsel)

;; startup, other config
(toggle-scroll-bar -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(setq inhibit-startup-screen t)
(setq backup-directory-alist '(("." . "~/.emacs.backup/")))
(setq default-directory "~/../../")

;; autocomplete
(add-hook 'after-init-hook 'global-company-mode)
(with-eval-after-load 'company
  (add-to-list 'company-backends 'company-anaconda))

;; visual
(doom-themes-visual-bell-config)
(powerline-center-theme)
(set-frame-font "Consolas 11" nil t)
(setq-default cursor-type 'bar)
(global-hl-line-mode 1)
(smartparens-global-mode 1)
(show-paren-mode 1)
(setq show-paren-delay 0)
(add-hook 'python-mode-hook #'rainbow-delimiters-mode)
(smooth-scrolling-mode 1)

;; python major mode customizations
;; line numbers
(defun my-python-mode-hook ()
  (anaconda-mode)
  (linum-mode 1)
  (linum-relative-mode 1)
  (add-hook 'python-mode-hook 'my-python-mode-hook))
(my-python-mode-hook)

;; ivy
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(setq ivy-re-builders-alist
      '((swiper . ivy--regex-plus)
	(t      . ivy--regex-fuzzy)))

;; other kbd bindings
(global-set-key (kbd "C-x C-b") 'switch-to-buffer)
(global-set-key (kbd "C-x C-u") 'undo)
(global-set-key (kbd "C-/") 'undo)
(global-set-key (kbd "C-.") 'other-window)
(global-set-key (kbd "C-,") 'previous-multiframe-window)
(global-set-key (kbd "C-;") 'comment-line)
(global-set-key (kbd "C-?") 'uncomment-region)
(global-set-key (kbd "C-'") 'isearch-forward)
(global-unset-key (kbd "M-<down-mouse-1>"))
(global-set-key (kbd "M-<mouse-1>") 'mc/add-cursor-on-click)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C->") 'mc/mark-all-like-this)
(global-set-key (kbd "C-c m") 'mc/edit-lines)

;; auto stuff
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (solarized-dark)))
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(package-selected-packages
   (quote
    (ac-ispell flyspell-correct ace-flyspell auctex multiple-cursors magit company-anaconda company rainbow-delimiters smartparens spacemacs-common)))
 '(smartparens-global-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:foreground "#586e75" :slant italic)))))
(set-cursor-color "#ff0000")

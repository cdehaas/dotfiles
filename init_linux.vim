" init.vim - neovim init for linux

syntax enable
filetype indent on
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
"set number
set relativenumber
set showcmd
set cursorline
set wildmenu
set showmatch
set autoindent
set visualbell
set nostartofline
set confirm
set ruler
set mouse=a
set ignorecase
set smartcase
set hlsearch
set incsearch

" Plugins
call plug#begin('~/.vim/plugged')
	Plug 'rafi/awesome-vim-colorschemes'
	Plug 'Kana/vim-smartinput'
	Plug 'junegunn/fzf'
	Plug 'junegunn/fzf.vim'
	Plug 'itchyny/lightline.vim'
	Plug 'terryma/vim-multiple-cursors'
	Plug 'tpope/vim-surround'
	Plug 'scrooloose/nerdtree'
	Plug 'scrooloose/nerdcommenter'
	Plug 'w0rp/ale'
	Plug 'airblade/vim-gitgutter'
	Plug 'Shougo/deoplete.nvim'
	Plug 'zchee/deoplete-jedi', {'do': ':UpdateRemotePlugins'}
	Plug 'vim-syntastic/syntastic'
	Plug 'maxbrunsfeld/vim-yankstack'
	Plug 'jceb/vim-orgmode'
call plug#end()

" Colorscheme
colorscheme flattened_dark

" Keybindings
nnoremap Y y$
nnoremap <leader>i :e ~/.config/nvim/init.vim
nnoremap <leader>t :NERDTreeToggle<CR>
nnoremap <leader>g :GitGutterToggle<CR>
nnoremap <leader>` :vsp<CR>:terminal<CR>
nnoremap <leader>\ :noh<CR>
nnoremap <silent> <C-k> :wincmd k<CR>
nnoremap <silent> <C-j> :wincmd j<CR>
nnoremap <silent> <C-h> :wincmd h<CR>
nnoremap <silent> <C-l> :wincmd l<CR>
inoremap jk <Esc>
inoremap kj <Esc>
tnoremap <Esc> <C-\><C-n>

" ALE
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_linters = {
\   'python': ['pep8', 'pylint'],
\   'python': ['pylint', 'flake8'],
\   'c': ['gcc'],
\   'perl': ['perl'],
\   'tcl': ['nagelfar'],
\   'verilog': ['verilator'],
\   'vim': ['vint'],
\   'markdown': ['markdownlint'],
\}

" Deoplete
let g:deoplete#enable_at_startup = 1

" Lightline
let g:lightline = {
\   'colorscheme': 'solarized',
\}

" NERDcommenter
let g:NERDSpaceDelims = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1